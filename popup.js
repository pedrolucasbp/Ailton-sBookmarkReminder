// // Copyleft by Pedro Lucas <pedrolucasbp@gmail.com>.
// Use of this source code is governed by a GPL license that can be
// found in the LICENSE here: https://www.gnu.org/licenses/gpl.html

/**
 * Main object with two random simplified bookmarks to show in
 * notifications and popup. The number two come from limitation
 * from two buttons in the Basic Notification format.
 */
RandNodes = [
    {title: "Clica aqui pra ver um favorito aleatorio que achei aqui", url: "", date: null},
    {title: "e aqui pra ver outro.", url: "", date: null}
];

/**
 * Search strings for find bookmarks with chrome.bookmarks.search method
 */
SearchStrings = ["http", "https","*.com","*.br"];

/**
 * @param Notification boolean depending on where this
 * method was called we want show or not the Notification.
 * The main method that find two random bookmarks 
 * and call for buildPopup() and showNotification()
 */
function findRandomNodes(Notification,attempts) {

    attempts = (typeof attempts !== 'undefined') ? attempts : 0;

    chrome.bookmarks.search(
    SearchStrings[Math.trunc(Math.random() * SearchStrings.length)],
    function(result){
        if(result.length > 0){
            rand = Math.trunc((Math.random() * result.length) + 1);
            RandNodes[0].url = result[rand].url;
            RandNodes[0].date = result[rand].dateAdded;
            console.debug("RandNodes[0].url = " +result[rand].url);

            rand = Math.trunc((Math.random() * result.length) + 2);
            RandNodes[1].url = result[rand].url;
            RandNodes[1].date = result[rand].dateAdded;
            console.debug("RandNodes[1].url = " +result[rand].url);

            var bmkDate = new Date(RandNodes[0].date);

            buildPopup(bmkDate);
            if(Notification)
                showNotification(bmkDate);
        }else{
            if(attempts < 3)
                findRandomNodes(Notification,attempts += 1);
            else
                console.log("I can't find bookmarks! Sorry... :/");
        }
    });
}
/**
 * @param bmkDate it's Date Object of first bookmark found 
 * by findRandomNodes().
 * Show a Chrome Notification when a new bookmark is made.
 */
function showNotification(bmkDate){
    MessagePool = [
        "TEM FAVORITO DE "+bmkDate.getFullYear()+" QUE MADAME AINDA NEM LEU!!!",
        "LEIA PRIMEIRO OS FAVORITO ACUMULADOS, DAMA!",
        "TEM UNS FAVORITO PRE-HISTORICOS AQUI hahahahaha",
        "A MADAME VAI DEIXANDO TUDO PRA DEPOIS E QUANDO VAI LER O SITE NEM EXISTE MAIS, NE DONA BONITONA?",
        "AI AI, SENHORITA..."
    ];
    var nOptions = {
        type:       "basic",
        title:      "Olha aqui:",
        message:    MessagePool[Math.trunc(Math.random() * MessagePool.length)],
        iconUrl:    "girl.png",
        buttons: [
            {title: RandNodes[0].title},
            {title: RandNodes[1].title}
        ],
        isClickable: true
    }
    chrome.notifications.create(nOptions, function(){})
}
chrome.notifications.onButtonClicked.addListener(function(notificationId,buttonIndex) {
        if (buttonIndex === 0) 
            chrome.tabs.create({url: RandNodes[0].url})
        if (buttonIndex === 1) 
            chrome.tabs.create({url: RandNodes[1].url})
});
/**
 * @param bmkDate Date object of first bookmark found 
 * by findRandomNodes().
 * Building popup from popup.html page.
 */
function buildPopup(bmkDate){
    document.getElementById('bkmdate').innerHTML = bmkDate.getFullYear();
    
    htmlLink1 = "Clica <a target='_blank' href='"+RandNodes[0].url+"'>aqui</a> pra ver um favorito alet&oacute;rio que achei aqui";
    document.getElementById('link1').innerHTML = htmlLink1;
    
    htmlLink2 = "e <a target='_blank' href='"+RandNodes[1].url+"'>aqui</a> pra ver outro.";
    document.getElementById('link2').innerHTML = htmlLink2;
}
// show reminder when a new bookmarks was created.
chrome.bookmarks.onCreated.addListener(function(id,bookmark){
    findRandomNodes(Notification=true);
});
// Building the popup message when the Extension is loaded.
document.addEventListener('DOMContentLoaded', function(){
    findRandomNodes(Notification=true);
});

