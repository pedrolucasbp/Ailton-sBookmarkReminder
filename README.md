# Ailton-sBookmarkReminder Chrome Browser Extension :p
## About
This Google Chrome Extension remind users in a funny way  
about yours bookmarks. Made only for fun to a friend. :D  
  
Sorry, but messages are only in Brazilian Portuguese (Yet).  

## Motivation
![Motivation](img/motivation.png)

## Install
 https://chrome.google.com/webstore/detail/ailtons-bookmark-reminder/gdhmaalcicpdgahkbjphddfgibhdmcmn

## Dev (pt-br)
1 - Download:

https://github.com/pedrolucasbp/Ailton-sBookmarkReminder/archive/master.zip  
  
2 - Descompacte em algum lugar no seu pc;  
  
3 - Vai no Chrome, clica nos três pontinhos > Mais Ferramentas > Extenções  * 
    Habilite o "Modo desenvolvedor" (no topo da página, um pouco a direita);  
    Clique em "Carregar extenção expandida...";  
    Encontre o diretorio do passo 2;  
  
4 - Aperte Ctrl+D em alguma página e veja!
  

## Project Team
* @ailtonmesquita
* @pedrolucasbp

## License
GPL https://www.gnu.org/licenses/gpl.html   
Sources at: https://gitlab.com/pedrolucasbp/Ailton-sBookmarkReminder
